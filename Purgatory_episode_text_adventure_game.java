import java.util.Scanner ;
import java.util.Formatter;

 // Everything EXCEPT console output printing lines, precisely and only specifically those containing game
 // character's dialogues, game world level's descriptions (in short - artistic imagination work of game author)
 // is licensed under Apache 2.0 license giving great freedoms to use game-engine code itself freely.
 // (Intars Kocesevs, 10 Dec 2021)
 
//---------------------------------------------------------------------------
/* Copyright 2021 Intars Kocesevs
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *   
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//---------------------------------------------------------------------------

/* Initially and partially created on February 2018 in paralel to computer science studies in Riga Technical University.
 * Development continued starting from November 2021, after finising studies.
 * ---------------------
 * It is text-based adventure game in which players explores mysterious Purgatory world.
 * This game conceptually is based on one of authors very special, long term artistic project codenamed 'Whm'.
 * Game can be looked upon as a branched storyline of one additional character that happened to 'get into' Purgatory.
 * As a player You are trying to figure out character's full story.	
 * ---------------------
 * (Mathematics paper and formula content taken from - A.N. Kovancov, L.V. Kovancova "Zanimatelnaja Istorija Matematiki"; 132 p.
 * Published in  Kiev, 2000)
 * --------------------------
 * (game and story author - Intars Kocesevs)	         */ 

public class Purgatory_episode_text_adventure_game
{
  
  Scanner data_entry ;									// variable will accept player's in-game choices (integer numbers)
  int life_energy ;										// variable measures players life energy
  String player_name ;
  String player_psychological_condition ;				// will hold word describing psychological state of player's character
  int player_choice ;                                   // variable will hold some currently made choice
  int player_location = 0;						     	// variable that old record of player's current position in levels world
  // due to inventory being String array, in a game, when player will try to use some items (like keys) to solve puzzles,
  // event of puzzle solving will involve comparison of user's chosen inventory actions (formated to String format) with 
  // a neccessary simbol sequence; but since player's choices in inventory itself at first are recorded as int numbers, 
  // at some point there must happen carefull cross-conversion back-and-forth from int-> String and vice versa;
  int[] action_signal_from_player = {0,0};	    	   // two number array will hold user choices with inventory items in level context
  boolean level_original_items_pick_up_event = false;	// will hold True/False for test whether player on particular map had picked up its items
  
  String[][] inventory = 
	  { {"1. ", "0", "            ", "2. ", "0", "            ", "3. ", "0"},
		{"4. ", "0", "            ", "5. ", "0", "            ", "6. ", "0"} };		// player inventory initialization
  
  int level1_revisit_count = 0;		// secret variable to count how many times player revisits very first level
  boolean secret_level3_entry = false;	// secret variable to hold a state of a player's visit into level 3
  boolean level5_atmospheric_hallway_first_visit_detection_state = true;	// variable is linked with milestone triggering event
  String key_enter = "will check if player pressed required letter-symbol key where it will be asked ";
  
  public static void main(String[ ] args)
  {
	  Purgatory_episode_text_adventure_game game_process_1 ;
	  game_process_1 = new Purgatory_episode_text_adventure_game( ) ;  // main, player controlled game class
	  game_process_1.title_screen( );
	  game_process_1.player_setup( );
	  game_process_1.level1_entry_into_purgatory( ) ;
  }
  
  public void title_screen()
  {
	  System.out.println("                                                     \n"
	  					+ " -------------------------------------------------------\n"
	  					+ "|           Purgatory episode   (demo-game)             | \n"
	  					+ "|                                                       | \n"
	  					+ "|                                                       | \n"
	  					+ "|-------------------------------------------------------| \n"
	  					+ "| game programming/ story/ concept                      | \n"
	  					+ "| (c) by Intars Kocesevs (Dec 2021)                     | \n"
	  					+ " ----------------------------------                       \n");
	  
	  System.out.println("!-----------------------------------------------------! \n"
	  					+ " Carefull: game demands carefull, only correct inputs \n"
	  					+ " under presented options! Read and play carefully.    \n");
	  
	  System.out.println(" ..........   Prologue   .................... \n"
	  		+ " When you regain self-awareness and full consciousness \n"
	  		+ " you discover yourself in a completely unknown, unfamiliar \n"
	  		+ " place. You don't remember even how you got here. \n\n");
  }
  
  public void player_setup( )
  {
    life_energy = 19 ;
    player_psychological_condition = "confused" ;
        
    System.out.println( "Life energy (health): " + life_energy + " (out of 50)") ;
    System.out.println( "Your psychological condition: " + player_psychological_condition) ;
    //System.out.println( "Inventory: " + inventory) ;		// was planned as a way to output initial inventory;
    System.out.println("You have no items with you");
    data_entry = new Scanner(System.in) ;
    System.out.println( "Enter your character's name:") ;
    player_name = data_entry.nextLine( ) ;
    System.out.println( "\n" + player_name + ", your quest has begun.");
  }
  
  public void player_opens_inventory(int[] level_context_possibilities, int current_location)
  {
	  // function works upon |String[][] inventory| global variable
	  String item_code = "0";
	  int i = 0; int j = 0;
	  for(j = 0; j <= 1; j++)
	  {
	    i = 0;
	    do
	    {
	      if( i == 1 || i == 4 || i == 7)
	      {
	        item_code = inventory[j][i];
	        switch(item_code)
	        {
	          case "0":
	            System.out.print(" ------- ");
	            break;
	          case "1":
	            System.out.print("paper from some math book");
	            break;
	          case "2":
	        	System.out.print("key to some door");
	        	break;
	          case "3":
		       	System.out.print("Abel's given key");
		       	break;
	          case "4":
		        System.out.print("key to door X");	// not implemented in this demo; this is placeholder
		        break;
	          case "7":
		        System.out.print("book");	// not implemented in this demo; this is placeholder
		        break;
	          default:
	            System.out.print("????");
	            break;
	        }
	        i++;
	      }
	      else
	      {
	        System.out.print( inventory[j][i]);
	        i++;
	      }
	    
	    } while ( i <= 7);
	    System.out.println(); 
	  }
	  
	  // now present inventory action choices
	  System.out.println("\nPress key for inventory actions:");
	  System.out.println("1-USE		2-DROP		3-CHECK		4-DO NOTHING");
	  player_choice = data_entry.nextInt( ) ;
	  switch (player_choice)
	  {
	  	case 1:
	  	 System.out.println("\nwhich item to use (1 to 6):");
	  	 player_choice = data_entry.nextInt( ) ;
	  	 action_signal_from_player[0] = 1;
	  	 // below a character / symbol of item's code number is being converted into respective numeric value;
	  	 // but first, we select in inventory array a respective indexed cell according to a user's input;
	  	 if (player_choice <= 3) {	j = 0;	}	// if player selected items Nr.1-3, it is inventory's first row
	  	 else	{	j = 1;	}					// else, if player selected items Nr.4-6, it is inventory'd second row
	  	 
	  	 // code below works with case when there is no item in slot that was chosen by
	  	 // user (let's say - accidentally); to prevent error, this case must be processed;
	  	 // there is a need to calculate which slot (column in inventory data structure) user chose;
	  	 // for first row items column number can be obtained by formula below:
	  	 int column_number = 0;
	  	 if ( j == 0) {	column_number = (player_choice * 3) - 2;	}	
	  	 // with second row's choices (4.-6. item) column numbers are obtained as follows:
	  	 if ( j == 1)
	  	 {
	  		 if (player_choice == 4) { column_number = 1; }
			 if (player_choice == 5) { column_number = 4; }
			 if (player_choice == 6) { column_number = 7; }
	  	 }
	  	 String users_picked_items_slot = inventory[j][column_number];
	  	 String symbols_for_inventory_empty_slot = "0";
	  	 if (users_picked_items_slot.equals(symbols_for_inventory_empty_slot))
	  	 {
	  		 System.out.println("\nThere is no item in this slot.");
	  		 break;
	  	 }
	  	 try {	action_signal_from_player[1] = Integer.valueOf(inventory[j][column_number]);	}
	  	 catch (NumberFormatException exception_with_number_conversion)
	  	 {	exception_with_number_conversion.printStackTrace();	}
	  	 // below is a test for conformity of a players choice of item action to the puzzles requirements;
	  	 level_context(action_signal_from_player, level_context_possibilities, current_location);
	  	 break;
	  	 
	  	case 2:
	  		System.out.print("\nwhich item to drop (1 to 6)?   ");
	  		player_choice = data_entry.nextInt( ) ;
	  		action_signal_from_player[0] = 2;
	  		action_signal_from_player[1] = player_choice;
	  		// inventory drops not implemented
	  		System.out.println("\n  -------------  !!!   --------------");
	  		System.out.println("Item drops not implemented currently");
	  		break;
	  	
	  	case 3:
	  		System.out.println("\nwhich item to check (1 to 6):");
		  	int index_for_item = data_entry.nextInt( ) ;
	  		item_check_in_inventory(index_for_item);
	  		break;
	  	
	  	case 4:
	  		player_choice = 0;
	  		break;
	  	default:
            System.out.print("????");
            break;
	  }
	  
  }
  
  public void item_pick_into_inventory(int picked_up_item_number_in_game_world)
  {
	  int signal_code_of_received_item = picked_up_item_number_in_game_world;
	  // need to check if inventory is full already
	  // otherwise:
	  
	  player_inventory_update (signal_code_of_received_item);
  }
  
  public void item_check_in_inventory(int player_choice_for_items_number_to_be_checked)
  {
	  int[] default_non_action_in_inventory = {0,0};
	  // below variable is meant to temporaly assign abstract location for player during menu operations
	  // so inventory opening and managing function/method can get all variables that it needs to work
	  int abstract_location_during_menu_actions = 0;	 
	  int row_number = 0; int column_number = 0;
	  if (player_choice_for_items_number_to_be_checked <= 3)
	  {	
		  row_number = 0;
		  // formula below calculates respective column number in player's inventory array (in 6 item model)
		  column_number = (player_choice_for_items_number_to_be_checked * 3) - 2;
	  }
	  if (player_choice_for_items_number_to_be_checked > 3 && player_choice_for_items_number_to_be_checked <= 6)
	  {
		  row_number = 1;
		  if (player_choice_for_items_number_to_be_checked == 4) { column_number = 1; }
		  if (player_choice_for_items_number_to_be_checked == 5) { column_number = 4; }
		  if (player_choice_for_items_number_to_be_checked == 6) { column_number = 7; }
	  }
	  	  
	  
	  String item_code_number = inventory[row_number][column_number];
	  // now, according item's description is taken and output to player
	  switch (item_code_number)
	  {
	  		case "0":
	  			System.out.println("\n-------------------");
	  			System.out.println("There is no item.");
	  			break;
	  		case "1":
	  			System.out.println("|   ..there exist triangles who's sum of angles, thus, can be larger then 180 degrees.                  |\n"
						+ "|    In this new plane space, of course, triangles with angles sum smaller then 180 can exist as well.  |\n"
						+ "|  5th Euclid axiom in this new geometry will still hold true, only in different way. In my analysis    |\n"
						+ "|  previously I shown how it happens and what constituted main initial difficulty in my efforts of      |\n"
						+ "|  new geometry theory foundations development and research.                                            |\n"
						+ "|                                                                                                       |\n"
						+ "|  Among non-intersecting lines there are such, which i call paralel ones, which infinitely approaches  |\n"
						+ "|  each other on one direction and infinitely moves away from each other in another direction. Here     |\n"
						+ "|  are those straight lines.                                                                            |\n"
						+ "|                                                                                                       |\n"
						+ "|  -._ B                                                                                                |\n"
						+ "|      --._                                                                                             |\n"
						+ "|       |   --._                                                                                        |\n"
						+ "|       |   _.^  --._                                                                                   |\n"
						+ "|       |-**          --._                                                                              |\n"
						+ "|    X  |   alpha          --._                                                                         |\n"
						+ "|       |                       --._    b                                                               |\n"
						+ "|       |                            --._    \\                                                          |\n"
						+ "|       |                                 --._\\                                                         |\n"
						+ "|       |                                 ----- .                                                       |\n"
						+ "|       |                                                                                               |\n"
						+ "|       |--.                                                                                            |\n"
						+ "|       |   *.                               ---_                                                       |\n"
						+ "|  ----------------------------------------------_>                                                     |\n"
						+ "|      A                                a    ..--                                                       |\n"
						+ "|                                                                                                       |\n"
						+ "|  If we draw a perpendicular from some point of one line to another, then appears angle alpha, which   |\n"
						+ "|  i call an angle of paralelity at perpendicular X. This angle is dependent from a length of a         |\n"
						+ "|  perpendicular:                                                                                       |\n"
						+ "|             _                                                                                         |\n"
						+ "|    alpha = | | (X).                                                                                   |\n"
						+ "|                                                                                  _                    |\n"
						+ "|  The longer is X, the smaller is angle. I also discovered a formula, expressing | |(X) through X:     |\n"
						+ "|                                                                                                       |\n"
						+ "|     _               -x/R                                                                              |\n"
						+ "|    | |(X) = 2arctg e                                                                                  |\n"
						+ "|                                                                                                       |\n"
						+ "|  From here we see that for X approaching values close to infinity o0, the angle of paralelity         |\n"
						+ "|  approaches value of 0, and we have.....         [ page is ripped here ] ---------------------------- |\n"
						+ " ---------------------------------------------------- \n");
	  			
	  			// command line just returns player back to inventory view
	  			player_opens_inventory(default_non_action_in_inventory, abstract_location_during_menu_actions);	
	  			break;
	  		case "2":
	  			System.out.println(" SPECIAL KEY ");
	  			break;
	  		
	  		case "3":
	  			System.out.println("\nOrdinary looking key which you received from Abel.");
	  			break;
	  		default:
	  			System.out.print("????  inventory glitch");
	            break;
	  }
  }
  
  public void player_inventory_update(int picked_up_item_code_number)
  {
	  int column_index = 1; int row_index = 0;
	  // check for first free slot
	  // check is being done on 2x3 (6 item) inventory model; 
	  while (inventory[row_index][column_index] != "0" && column_index < 7)
	  {		  column_index += 3;	  }
	  
	  String last_looked_items_slot = inventory[row_index][column_index];
	  if (last_looked_items_slot != "0")
	  {
		  column_index = 1; row_index = 1;
		  while (inventory[row_index][column_index] != "0" && column_index < 7)
		  {		column_index += 3;	}
	  // conversion of item code number into string value to fit into nventory string array model;
	  inventory[row_index][column_index] = String.valueOf(picked_up_item_code_number);		  		  
	  }
	  
	  else
	  {
		  inventory[row_index][column_index] = String.valueOf(picked_up_item_code_number);	
		  int found_free_slot_row_index = row_index;
		  int found_free_slot_column_index = column_index;
	  }
  }
  
  public void level_context(int[] player_inventory_action_signal, int[] level_puzzles_action_possibilities, int current_level)
  {
	  // function holds levels puzzle interaction management
	  // ---------------------------------------------------
	  switch (current_level)
	  {
	  	
	  	case 2:		// for level 2: door D1
		  if (player_inventory_action_signal [0] == level_puzzles_action_possibilities[0]
				  && player_inventory_action_signal[1] == level_puzzles_action_possibilities[1])
		  {
			  System.out.println("\n  Door is opened now!");
			  secret_level3_entry = true;	// register the event of player entering this secret level
			  level3_behind_door_D1( );
		  }; break;
		  
		
		case 8:		// for level 8: door D-8B in great hall 
			if (player_inventory_action_signal [0] == level_puzzles_action_possibilities[0]
					  && player_inventory_action_signal[1] == level_puzzles_action_possibilities[1])
			{
				System.out.println("---------------------------------------------------------------- \n"
						+ "  Door unlocked! Abel's key turned out to be a correct for it.");
				level10_the_road_somewhere();
			} break;
			
		default:
			// message in case if function skips any puzzle cases by some reason;
			System.out.println("\n Message: function that works with levels puzzle contexts reverted to default case."
					+ "Something happened in level_context() function.");
	  	
	  }
	  
  }
  
  public void level1_entry_into_purgatory( )
  {
    System.out.println( "\n--------------------------------------------------------------------------------------") ;
    System.out.println( "You are in a narrow entrance corridor of a Purgatory. It looks like a basic motel reception's room.");
    System.out.println( "Two almost identical persons stand behind reception desk's counter. They flip through some book pages.");
    System.out.println( "What you choose to do?");
    System.out.println( "-----------------------------");
    System.out.println( "1: Talk with both of them.");
    System.out.println( "2: Talk with person at your left.");
    System.out.println( "3: Talk with person at your right.");
    System.out.println( "4: Try to provoke heated debate and clash. Create conflict.");
    System.out.println( "5: Bypass them calmly as if nothing happened and proceed to the only one - left turn in corridor.");
    
    player_choice = data_entry.nextInt( ) ;
    
        
    if ( player_choice == 1)
    {    System.out.println( "\nNone of them respond and they continue their actions.") ;
    	 level1_entry_into_purgatory( );
    }
    
    if ( player_choice == 2)
    {
      System.out.println( "-------------------------------------------------------------------------------------------\n"
    		  			+ "                   Left subject: Greetings to bypasser. You probably want to ask something?\n");
      System.out.println( "\nYou: \n- What is this place, what is with my memories, why i hardly can recall\n"
    		  			+ " how i got here? What am i doing here? Who are you, what do you need\n"
    		  			+ " from me?\n");
      System.out.println( "                   Left subject: How do we supposed to know? We don't know, who you are and\n"
    		  			+ "        			 how you got here. Would be curious to get to know ourselves.\n"
    		  			+ "				 We are not looking for anything from you. We are archivists\n"
    		  			+ "				 and we keep the journal of visit events as well cooperate \n"
    		  			+ "				 with few other archives besides some other duties.\n");
      
      System.out.println( "\nYou (left somewhat speechless): -----?              ");
      level1_entry_into_purgatory( );
    }
    
    if ( player_choice == 3)
    {
    	System.out.println( "\nRight subject doesn't seem to respond in any way.");
    	level1_entry_into_purgatory( );
    }
    
    if ( player_choice == 4)
    {
    	System.out.println( "--------------------------------------------------------------------------------------------");
        System.out.println( "You try to jump over desk's counter. Both receptionists react more quickly than you expected");
        System.out.println( "and would wish. You have been grabbed and forced into inactive posture, yours hands firmly ");
        System.out.println( "caught and held behind your back. One of receptionists takes out from a drawer a keys bundle");
        System.out.println( "and the next second you feel how heavy keys bundle hits your head. It was quite heavy.");
        System.out.println( "Your desire for conflict and for bothering others immidiately diminishes. You are being pushed");
        System.out.println( "away and thrown back over the desk's counter.\n");
        System.out.println( "	  One of the receptionists: It is not a way of how to start a less painful walk in Purgatory.\n");

        life_energy -= 1 ;
        test_for_game_over_event(life_energy);
        if (life_energy > 0)        {	level1_entry_into_purgatory();	}
        
     }
    
     if ( player_choice == 5)
     { 
    	System.out.println( "--------------------------------------------------------------------------------------------");
    	System.out.println( "Some sort of uneasiness glooms in your mind but at the same time you feel somewhat more determined.");
    	System.out.println( "Hallway has a parquet floor. There are no windows but artificial lighting is sufficient");
    	System.out.println( "to see anything all around. You decide to proceed to the end of the corridor where a single");
    	System.out.println( "door is seen.");
    	
    	level1_entry_into_purgatory_exit_doors( ) ;
     }
     
  }
  
  public void test_for_game_over_event(int player_energy_status)
  {
	  if (player_energy_status <= 0)
	  {	  System.out.println("--------------------------------------------------------------------------------------------");
		  System.out.println( player_name + ", your life energy has decreased critically. You cease to exist as an active,\n"
			  				+ "autonomous body in space of Purgatory and slowly start to become a passive, integrated\n"
			  				+ "and a collected information field of whole Purgatory space-pause-time.\n\n"
			  				+ "      Your active adventure has ended");	}
	  else
	  {	System.out.println( "Health: " + life_energy + " (out of 50)");	}
	  
  }
 
  public void level1_entry_into_purgatory_exit_doors( ) 
  {
	  	System.out.println( "--------------------------------------------------------------------------------------------\n"
	  						+ "You approached the door. On it can be seen small plate with some symbols - three black cubes:\n"
	  						+ "                                                                                          \n"
	  						+ "                                                          ,-----,        ,-----,          \n"
    						+ "                                                         /_|__ /|       /_|__ /|          \n"
    						+ "                                                         | /  | /       | /--|-/          \n"
    						+ "                                                         |/___|/        |/___|/           \n"
    						+ "                                                                ,-----,        .          \n"
    						+ "                                                               /_|__ /|      .            \n"
    						+ "                                                               | /  | /                  \n"
    						+ "                                                               |/___|/       .     .     \n"
    						+ "\n"
    						+ "Door turns out to be opened, it is not closed by any key mechanism. You can open it.\n"
    						+ "---------------------------------\n"
    						+ "1: Open the door and go further. \n"
    						+ "2: Return to reception hallway and try to get to know something more.");
	  			
	   	player_choice = data_entry.nextInt( ) ;
	   	
    	if ( player_choice == 1)    {	level2_corridor_Nr1(level_original_items_pick_up_event = false );  	     }
    	
    	if ( player_choice == 2)	{	level1_revisit_count += 1; level1_entry_into_purgatory_revisited( );	}
   }
    
    public void level1_entry_into_purgatory_revisited( )
    {
    	player_choice = 0;
    	player_location = 1;
    	int[] level1_context_possibilities = {0,0};
    	
    	// below is a quick check for secret
    	if (level1_revisit_count == 4) {	level1_secret(); }
    	System.out.println( "\n--------------------------------------------------------------------------------------") ;
        System.out.println( "You find that receptionists are nowhere to be found anymore. It is as if they dissappeared.\n"
        					+ "They even left their book, lights on. You go to the door from which you entered into this \n"
        					+ "place to begin with and open it only to find ..that there is nothing beyond it! Just total,\n"
        					+ "complete darkness with no traces of any roads, no concrete ground.\n"
        					+ "-----------------------\n"
        					+ "1: Return back to unexplored, next corridor.\n"
        					+ "9: Check your inventory.\n");
        
        player_choice = data_entry.nextInt();
        
        if (player_choice == 1)		{	level2_corridor_Nr1 (level_original_items_pick_up_event);		}
        if (player_choice == 9)
        {
        	player_choice = 0;	// reset for future use
        	player_opens_inventory(level1_context_possibilities, player_location);
        	level1_entry_into_purgatory_revisited( );
        }
        player_choice = 0;	// reset value for safe next usage
    }
    
    public void level1_secret()
    {
    	level1_revisit_count += 1;
    	System.out.println( "\n--------------------------------------------------------------------------------------") ;
    	System.out.println("You find that receptionists are nowhere to be found anymore. It is as if they dissappeared.\n"
    						+ "Suddenly it occured to you to use that moment and show off some curiosity around the \n"
    						+ "place itself. You jump over the desk counter and start to examine the place. You take a \n"
    						+ "look at book that archivists were working with - it is filled with tabulated entries, \n"
    						+ "some kind of marks, like numbers and comments as 'this key was found broken. Examine \n"
    						+ "what this means to its door and path behind it, whether it can be opened and safely entered \n"
    						+ "without physical anomalies occuring and whether space remains the same and leads to the same \n"
    						+ "explored teritory'. And others, like 'found catalogue probably could be atributed to zone ...\n"
    						+ "(you see some kind of number and symbol sequence) but was moved by somebody for some purpose'.\n"
    						+ "In worries that archivists may reappear just as suddenly and not wishing, by some reason, be \n"
    						+ "caught sneaking around like that, in a hurry you examine wall and space beneath the desk counter. \n"
    						+ "You look for anything in the wall that could reveal itself to be camouflaged doors through which \n"
    						+ "archivists most likely (as you would like to think) dissappeared and went somewhere unnoticed.\n"
    						+ "Yet you find no signs of any such doorway. Beneath the desk you do find a drawers and one is \n"
    						+ "easily opened.\n\n"
    						+ "  You find a key that looks somewhat special and has symbol markings on it! \n"
    						+ "  It has some kind of blue, small gem on it, too. \n\n"
    						+ "You take it into your pockets of your jacket and quickly jump back over desk counter.");
    	
    	int item_level_1_secret_key = 2;
		item_pick_into_inventory(item_level_1_secret_key);
    }
    	    	
    public void level2_corridor_Nr1(boolean original_item_pickup_event )
    {
    	// at first there is some checking going on whether player already had been
    	// here previously and if whether he picked up this level's original, inherent item (paper);
    	player_location = 2;	// player is located in level 2
    	System.out.println( "-----------------------------------------------------------------------------------------\n"
    						+ "The corridor that you now see looks more like a war bunker. It is somewhat darker here \n"
    						+ "but still everything can bee seen. There is one turn and in whole corridor on one side \n"
    						+ "of the wall there is another one door to some place. Corridor ends with another door.");
    	String text_block_for_item_paper_on_level2_corridor_Nr1 = "On the floor you spot some old, crumbled paper.\n";
    	if (original_item_pickup_event == false)
    	{System.out.println(text_block_for_item_paper_on_level2_corridor_Nr1);}
    	    	
    	System.out.println
    			( "\n"
	  			+ "                                               .- - - - - - - - - - - - -              \n"
	  			+ "                               to next area < \\ D2                      |              \n"
	  			+ "                                               .- - - - - - - - -       |              ");
	  	if (original_item_pickup_event == true)
	  		// if player picked up paper item then level map display must be updated
	  		{ System.out.println("                                                                |       |              ");}
	  	else
	  	{ System.out.println("                                                                | papers|              ");}
    	
	  	System.out.println("                                                                |       |              \n"
	  			+ "                                                                |       |              \n"
	  			+ "                                                                /D1     |              \n"
	  			+ "                                                                |       |              \n"
	  			+ "                                                                |   y   |              \n"
	  			+ "                                                                |_/   __|              \n"
	  			+ "                                                                    v                  \n"
	  			+ "                                                       to level1 - \"reception\"       \n"
	  			+ "-----------------------------------\n"
	  			+ "1: Approach door on side wall (D1).");
	  	if (original_item_pickup_event == false)
	  	{	System.out.println("2: Check the papers.");	}
	  			
	  	System.out.println("3: Go to door D2.\n"
	  					+ "4: Return where it all started - to reception hallway.");
	  	    	
    	player_choice = data_entry.nextInt();
    	
    	if (player_choice == 1)
    	{
    		player_choice = 0;	// reset value for safe next usage
    		int[] level2_context_possibilities = {1,2};		// to [1]USE a [2]key
    		System.out.println( "---------------------------------------------------------------------------------------------\n"
								+ "The door seems very interesting - by its look it stands out in a bunker corridor environment.\n"
								+ "Door looks more like a door from some completely different architecture style, resembling  \n"
								+ "some sort of a living room/appartment's door done with artistic twist - it has a drawing of some\n"
    							+ "sort of a cartoon dog dressed in some magician clothing. Door is closed. \n"
    							+ "--------------------------------------------------------");
    		System.out.println("3: Forget this door and focus on corridor.\n"
    						 + "4: Return where it all started - to the reception hallway.\n"
    						 + "9: Check your inventory.\n");
    		player_choice = data_entry.nextInt();
    		
    		if (player_choice == 3) {	level2_corridor_Nr1(original_item_pickup_event);  	}
    		if (player_choice == 4) {	level1_revisit_count += 1; level1_entry_into_purgatory_revisited( );	}
    		if (player_choice == 9)
    		{
    			player_opens_inventory(level2_context_possibilities, player_location);
    			player_choice = 0;		// zero out to reset variable to prevent it from "leaking" through system
    			if (secret_level3_entry == false) {	level2_corridor_Nr1(original_item_pickup_event);	}
    			player_choice = 0;	// reset value for safe next usage
    		}
    		player_choice = 0;	// reset value for safe next usage
    		
    	}
    	
    	if (player_choice == 2)
    	{
    		System.out.println("\n----------------------------------------------------------------------------------------"
    						+ "\n By the look of it, page looks like a torn out page from some sort of geometry/math book.\n");
    		System.out.println("|   ..there exist triangles whos sum of angles, thus, can be larger then 180 degrees.                   |\n"
    						+ "|    In this new plane space, of course, triangles with angles sum smaller then 180 can exist as well.  |\n"
    						+ "|  5th Euclid axiom in this new geometry will still hold true only in different way. In my analysis     |\n"
    						+ "|  previously I shown how it happens and what constituted main initial difficulty in my efforts of      |\n"
    						+ "|  new geometry theory foundations development and research.                                            |\n"
    						+ "|                                                                                                       |\n"
    						+ "|  Among non-intersecting lines there are such, which i call paralel ones, which infinitely approaches  |\n"
    						+ "|  each other on one direction and infinitely moves away from each other in another direction. Here     |\n"
    						+ "|  are those straight lines.                                                                            |\n"
    						+ "|                                                                                                       |\n"
    						+ "|  -._ B                                                                                                |\n"
    						+ "|      --._                                                                                             |\n"
    						+ "|       |   --._                                                                                        |\n"
    						+ "|       |   _.^  --._                                                                                   |\n"
    						+ "|       |-**          --._                                                                              |\n"
    						+ "|    X  |   alpha          --._                                                                         |\n"
    						+ "|       |                       --._    b                                                               |\n"
    						+ "|       |                            --._    \\                                                          |\n"
    						+ "|       |                                 --._\\                                                         |\n"
    						+ "|       |                                 ----- .                                                       |\n"
    						+ "|       |                                                                                               |\n"
    						+ "|       |--.                                                                                            |\n"
    						+ "|       |   *.                               ---_                                                       |\n"
    						+ "|  ----------------------------------------------_>                                                     |\n"
    						+ "|      A                                a    ..--                                                       |\n"
    						+ "|                                                                                                       |\n"
    						+ "|  If we draw a perpendicular from some point of one line to another, then appears angle alpha, which   |\n"
    						+ "|  i call an angle of paralelity at perpendicular X. This angle is dependent from a length of a         |\n"
    						+ "|  perpendicular:                                                                                       |\n"
    						+ "|             _                                                                                         |\n"
    						+ "|    alpha = | | (X).                                                                                   |\n"
    						+ "|                                                                                  _                    |\n"
    						+ "|  The longer is X, the smaller is angle. I also discovered a formula, expressing | |(X) through X:     |\n"
    						+ "|                                                                                                       |\n"
    						+ "|     _               -x/R                                                                              |\n"
    						+ "|    | |(X) = 2arctg e                                                                                  |\n"
    						+ "|                                                                                                       |\n"
    						+ "|  From here we see that for X approaching values close to infinity o0, the angle of paralelity         |\n"
    						+ "|  approaches value of 0, and we have.....         [ page is ripped here ] -----------------------------|\n" 
    						+ "---------------------------------------------------\n\n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{	
    			
    			System.out.println(" - Maybe i should leave my pockets empty for some other future \n"
    						+ " items of more value - you think to yourself. - I am not quite \n"
    						+ " into math stuff right now. Maybe i better leave space in my \n"
    						+ " pockets and jacket for whatever i may find later, like keys, \n"
    						+ " than stuff it with random picked-up papers. Or better take it? \n");
    						
    			System.out.println("1: Pick up paper (into inventory).\n"
    					   	+ "2: Leave paper here and proceed with other options.\n");
    			player_choice = data_entry.nextInt();
    			if (player_choice == 1)
    			{
    				int item_level_2_paper = 1;
    				item_pick_into_inventory(item_level_2_paper);
    				original_item_pickup_event = true;
    				level_original_items_pick_up_event = true;
    				level2_corridor_Nr1(original_item_pickup_event); //
    			}
    			if (player_choice == 2)
    			{
    				level2_corridor_Nr1(original_item_pickup_event);
    			}
    		}
    		
    	}
    	
    	if (player_choice == 3)
    	{
    		player_choice = 0;		// resetting value
    		level4_room_with_curiosities();
    	}
    	
    	if (player_choice == 4)
    	{
    		player_choice = 0;		// resetting value
    		level1_revisit_count += 1;
    		level1_entry_into_purgatory_revisited( ); 
    	}
    
    }
    
    public void level3_behind_door_D1( )
    {
    	player_choice = 0;	// reseting the variable for game logic procesing safety
    	secret_level3_entry = true;
    	System.out.println("-------------------------------------------------------------------------------\n"
    			+ "You have entered a room where in one corner you see standing what seems like ancient \n"
    			+ "computer and some kind of telephonic system, also crude and somewhat archaic-looking. \n"
    			+ "You notice that device, thou, shows some signs of activity. In another corner you see \n"
    			+ "huge pile of various types of wires and some sort of hardware trash-pile. There is one \n"
    			+ "really worn out sofa here. You notice that there is some kind of wiring going out of \n"
    			+ "the ancient telephonic system and continuing into the opened doorway. You approach \n"
    			+ "the opened doorway hoping to see someone but instead of either corridor or some room \n"
    			+ "or stairway you get to see shapeless, structurless, dark grey-colored fog-like void \n"
    			+ "with no particularly recognizable distance or spatial reference-points like corners or \n"
    			+ "anything. Still, you do notice that in this fog there is visible some kind of an object \n"
    			+ "which did resemble something similar to humanoid form - almost as if a person's shape, \n"
    			+ "walking either from the doorway or to the doorway. At least shape could be viewed as if \n"
    			+ "there is a person in a walk moment  ...with the addition that it looked as if paused \n"
    			+ "or in infinitely slow motion. You felt as if by looking at it longer, you catched a \n"
    			+ "glimpse of a movements continuation although mind was conflicting in its decision if it \n"
    			+ "just appeared so.\n");
    	System.out.println("... press 'c' (and then 'Enter') to continue ...");
		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
		if (key_enter.equals("c"))
		{
			System.out.println("You throw one small object into this open doorway and you see how, at first, \n"
    			+ "it flies into it as under normal, expected physical behaviour and after brief moment \n"
    			+ "you notice how it's movement gradually slows down and object almost becomes still in \n"
    			+ "air. You decide that it is time to risk and try to use possible supernatural properties \n"
    			+ "of this whole place and see where it leads. You step into this void. Immediately you \n"
    			+ "feel as if you are carried forward by some force without any of your efforts. You turn \n"
    			+ "around but you still can see the open doorway behind you. In front of you the person's \n"
    			+ "shape hasn't changed. It still looks like something blurry and hard to describe. But you \n"
    			+ "feel that you can make movements and go in any direction. You can even slightly but not \n"
    			+ "much go 'up'. You start your journey into unknown and, first, you think that you will follow \n"
    			+ "the telephone line cables that you kind of ...spotted. If there is some kind of communication \n"
    			+ "lines, that might mean there is some modern ways of dealing with things and problems here.\n");
    	System.out.println("            ...........                \n");
    	System.out.println("Game ending stats:  \n"
    			+ "player name: " + player_name + "\n"
    			+ "ending scenario: B");
		}
    	
    }
    
    public void level4_room_with_curiosities()
    {
    	player_location = 4;
    	int[] level4_context_possibilities = {0,0};		// for now there is no contextual puzzle-solving actions
    	System.out.println("---------------------------------------------------------------------------------\n"
    					+ "This area looks like some sort of a writer's working room from 19th century. It looks so \n"
    					+ "different than the previous corridor or 'reception'. It is darker here but some sort of a\n"
    					+ "strange looking object, glowing out like dim lightbulb and put into some kind of bookshelf \n"
    					+ "at one corner, gives enough lighting to see the room. It is as if somebody had been here \n"
    					+ "recently and left the light on. \n");
    	System.out.println("                                                                                    \n"
    					+ "                                       _.------------------------------------------- \n"
    					+ "                        D-4A        _-o            \\ D2  > back to level 2-corridor \n"
    					+ "  -------------------|--___--|-----              ----------------------------------- \n"
    					+ "  | |book shelf|                        y     ---|                                   \n"
    					+ "  |                                           |  |                                   \n"
    					+ "  / D-4B                    .----------.      |  |                                   \n"
    					+ "  |                         |table_____|      |  |                                   \n"
    					+ "  |                         |___|             |__|                                   \n"
    					+ "  |                                chair         |                                   \n"
    					+ "  |                          _________   _______ |                                   \n"
    					+ "  |        curtains         |         | |       ||                                   \n"
    					+ "  --------- ^v^w-v^^ ----------------------------                                    \n");
    	
    	System.out.println("\n------------------------------------\n"
    					+ "1: Take a look around the room.\n"
    					+ "2: Approach door (D-4A).\n"
    					+ "3: Approach door (D-4B).\n"
    					+ "4: Return back (to previous area - corridor).\n"
    					+ "9: Check inventory.\n");
    	
    	player_choice = data_entry.nextInt();
    	
    	if (player_choice == 1)
    	{
    		System.out.println("-------------------------------------------------------------------------------------\n"
    						+ "You start walking around the room's space, taking notice of objects and \n"
    						+ "their placements as you walk by. At one corner you see some book shelf - almost \n"
    						+ "full of chaotically stacked books. At quick glance on some of their covers and \n"
    						+ "titles they represent some sort of encyclopedias, natural science books, something \n"
    						+ "about astronomy, astrophysics and also some comic books about ninja warriors and \n"
    						+ "some cartoon characters space adventure pack. All in all, nothing of particular and \n"
    						+ "immediate interest to you. Your mind is in a state of psychological confusion from \n"
    						+ "everything that happened so far with you, and you don't feel enough willpower to \n"
    						+ "dig into details of this book shelf's contents. You notice that there are no windows \n"
    						+ "in this room although there is something resembling modern wall sockets for light or \n"
    						+ "some device. You wonder -- 19 century cabinet with an electric sockets? You proceed \n"
    						+ "towards the table. There are some pen and papers and artifacts like zooming lense \n"
    						+ "on it. Table has some drawers - you open each of them. All drawers are open, you \n"
    						+ "check them to find that they are stacked with various form papers, there is even \n"
    						+ "few X-ray photocopies of some human spine and skull region. Papers are written in \n"
    						+ "unfamiliar language. In one drawer you found a sack that contained some sort of a \n"
    						+ "ancient-looking board game with game accessories and keys in form of cartoonish \n"
    						+ "animals like rabbit, cat and others. At a quick glance you don't discover anything \n"
    						+ "that might be of interest to you - like some door keys or maps of place. And so you \n"
    						+ "finally turn your attention to curtains hiding something behind them.  -Is it probably \n"
    						+ "some window? - you wonder. You decidedly approach curtains and slide them to the side.\n"
    						+ "There you see a stairs leading somewhere.\n");
    		System.out.println("------------------------------------\n"
					+ "1: Take a risk and explore where they lead.\n"
					+ "2: Think for a moment and decide to explore at some later time. Return attention to cabinet room.\n");
    		
    		player_choice = data_entry.nextInt();
    		
    		if (player_choice == 1)
    		{
    			level7_special_observations_place();
    		}
    		
    		if (player_choice == 2)
    		{
    			level4_room_with_curiosities();
    		}
    	}
    	
    	if (player_choice == 2)		// if decision is to approach door D-4A (to level 5)
    	{
    		level5_atmospheric_hallway(level5_atmospheric_hallway_first_visit_detection_state);
    	}
    	
    	if (player_choice == 3)		// if decision is to approach door D-4B (to level 6)
    	{
    		level6_philosophers_room();
    	}
    	
    	if (player_choice == 4)    	{	level2_corridor_Nr1(level_original_items_pick_up_event);	}
    	if (player_choice == 9)
    	{
    		player_choice = 0;		// zero out to reset variable to prevent it from "leaking" through system
    		player_opens_inventory(level4_context_possibilities, player_location);
			level4_room_with_curiosities();
    	}
    }
    
    public void level5_atmospheric_hallway (boolean player_first_time_visit_check)
    {
    	player_choice = 0;	// reset variable to nullify possible interference of previous decisions
    	if (player_first_time_visit_check == true)
    	{
    		System.out.println("\n-------------------------------------------------------------------------------------\n"
    						+ "You are at what seems like quite long, high-ceiling, very light kings and queens era's reminiscent\n"
    						+ "hallway with one turn, meaning hallway is L-shaped. One side of this hallway in its segment before\n"
    						+ "first turn is a wall without any windows but the rest of the segments on both wall sides are like \n"
    						+ "windowed, glass walls.");
    		System.out.println("\n"
    						+ "                        xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx- \n"
    						+ "    to next area      < | D-7B                                         | \n"
    						+ "                        xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-.    | \n"
    						+ "                                                                  |    | \n"
    						+ "                                                                  x    | \n"
    						+ "                                                                  x    | \n"
    						+ "                                                                  x    | \n"
    						+ "                                                                  x    | \n"
    						+ "                                                                  |    | \n"
    						+ "                                                                  x    | \n"
    						+ "                                                                  x    | \n"
    						+ "                                                                  x y  | \n"
    						+ "                                                                  |_  _| \n"
    						+ "                                                                         \n"
    						+ "                                                                    v    \n"
    						+ "                                                        back to previous area \n\n");
    		System.out.println("Behind the windows you see that a total darkness encompasses everything on one side.\n"
    						+ "On opposite side windows, through their glass, you can see that some kind of gigantic \n"
    						+ "rock cliff with some sort of walking roads and some structures is at some distance. \n"
    						+ "The darkness encompasses everything there also but at some spots there is something \n"
    						+ "like dim lighting spots. Still, from here you can't see much details. In a hallway, \n"
    						+ "were both sides are windowed, you see a person standing and presumably thinking about \n"
    						+ "something. You steadily approach the person and can see that person looks like some \n"
    						+ "kind of a student, a young man.\n");
    		
    		// prepare dialogue text block incremental appearing for readability
    		System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{
    			System.out.println("\nYou: \n"
        				+ "Hey, fella, what are you doing here? Can you tell me \n"
        				+ "what this place is? I am kind of lost here.\n");
        		System.out.println("                      Person:  (looks at you) I am analyzing my thoughts. Good thing \n"
        				+ "                               this place allows plenty of such moments. As for question \n"
        				+ "                               what this place is - that will be harder to answer. You \n"
        				+ "                               are new individual here, it seems. Didn't see you before \n"
        				+ "                               in many time periods spent here. It is normal to be lost \n"
        				+ "                               here.\n");
        		System.out.println("You: \n"
        				+ "Am i hallucinating or in some sort of a dream? I don't \n"
        				+ "recognize this whole place, its not the world were i \n"
        				+ "was even yesterday for real. Are you real?\n");
        		System.out.println("                      Person:  I am real. And so do you. For what i know, you can allow\n"
        				+ "                               yourself to start accepting that yesterday or whatever\n"
        				+ "                               time ago you experienced death or maybe some near death\n"
        				+ "                               experience in your previous, 'real' world, as you most\n"
        				+ "                               likely will like to refer. Consider this as a best hypothesis\n"
        				+ "                               for now.\n");
    		}
    			
    		System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{
    			System.out.println("You: \n"
        				+ "Dead? But i am alive. And i have no recollection of memories \n"
        				+ "about my death event or anything traumatic. \n");
        		System.out.println("                      Person:  hardly any new arriving individuals remember at instant\n"
        				+ "                               their last moments of previous, 'real' existence. We think\n"
        				+ "                               that to some degree this is possible - a recovery of latest,\n"
        				+ "                               more closer to the death or near-death event memories and \n"
        				+ "                               circumstances back 'there'. But we don't know for sure. We\n"
        				+ "                               do know that this place holds, like an archive, only in a\n"
        				+ "                               different sense, at least some fragments of places with an\n"
        				+ "                               artifacts or their copies, should i say, that, once discovered\n"
        				+ "                               by you, could remind and revive you exact and precise previous\n"
        				+ "                               information. That, of course, if you are so willing to do that.\n");
        		System.out.println("You: \n"
        				+ "Alright, ee. kid... lets suppose i totally believe you on that. These \n"
        				+ "places or, as you phrased, copies of them - is it hard to find \n"
        				+ "them and if i find them, can i get back into real world, were i \n"
        				+ "was alive? \n");
        		System.out.println("                      Person:  in short you can call me Abel. About finding these places - \n"
        				+ "                               no one knows for sure. They can be anywhere, in totally random, \n"
        				+ "                               unconnected places. Some might be unreachable or nowhere to be \n"
        				+ "                               found. You can only be sure that at least some - one, two, maybe \n"
        				+ "                               more you will find if you seek long enough and stay acting-alive \n"
        				+ "                               here. I had found some of me-related ones. This place won't \n"
        				+ "                               pressure you anyhow to do that. You are almost as free individual \n"
        				+ "                               here in some sense.\n");
    		}
    		
    		System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{
    			System.out.println("You: \n"
        				+ "..(you think about what you just heard)..\n"
        				+ "Who are those \"we\" that you mention numerous times? Are here many \n"
        				+ "individuals? Or you refer to some managers of this place?\n");
        		System.out.println("                        Abel:  By \"we\" i mostly mean my master-teacher in geometry and math \n"
        				+ "                               science, mister Nikolai Lobachevsky whom i happened to meet \n"
        				+ "                               by accident and luckily once and few more colleagues-scientists \n"
        				+ "                               who joined us too at one point. Our guess is - there are no 'upper'\n"
        				+ "                               chiefs of any sort in this place except physics, space and time laws. \n");
        		System.out.println("You: \n"
        				+ "What are those archivists who waits you upon arrival? Are \n"
        				+ "they humans? There is something suspicious about them. I had \n"
        				+ "two, almost identical ones waiting for me.\n");
        		System.out.println("                        Abel:  Never met any archivists. But i did hear about them. Supposedly \n"
        				+ "                               they are humans, who being here for uncountable time periods and \n"
        				+ "                               who developed an interest into being some sort of historians for \n"
        				+ "                               this place. They can't dissappear anywhere, they are the same as \n"
        				+ "                               you or me, previous-life humans.\n");
        		System.out.println("You: \n"
        				+ "But i certainly...  (you think for a moment)...eh.. \n"
        				+ "forget about that. Kid, you look young to me. Too young \n"
        				+ "to be here, if that's the after-death-universe, so to speak.\n"
        				+ "An accident or something?\n");
        		System.out.println("                        Abel:  I don't know yet. I have been for so long here but still hadn't \n"
        				+ "                               found my main memory room full of artifacts that would remind me .\n"
        				+ "                               So far i only rediscovered that in previous life i was mathematician \n"
        				+ "                               working on important discovery of new theorems. There was hardly any \n"
        				+ "                               enemies to me. I have even some recollection of my developed theorems.\n"
        				+ "                               Mister Lobachevsky said that i am on to some very big discovery in \n"
        				+ "                               algerba. He teaches me a lot and we both research foundations of an \n"
        				+ "                               algebra and geometry.\n");
    		}
    		    		
    		System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{
    			System.out.println("You: \n"
        				+ "Sounds like at least someone may have fun times here, \n"
        				+ "after all. But still, is it possible to escape this place \n"
        				+ "somehow and get back to normal, previous life?\n");
        		System.out.println("                        Abel:  It is absolutely unknown to us. And mostly this place will be \n"
        				+ "                               like a deserted universe. It is so vast, almost infinite-like and \n"
        				+ "                               most likely you will rarely meet somebody other. It is as if this \n"
        				+ "                               place was made for lone quests in exchange for total existential \n"
        				+ "                               freedom, wandering on near cosmic scale and thinking. For a possible \n"
        				+ "                               exit door, if there exists such a thing, you will have to look for it by \n"
        				+ "                               yourself, on your own.\n");
        		System.out.println("You: \n"
        				+ "(A heavy pause in your thoughts. You think to yourself that \n"
        				+ "at least you got yourself a temporal mission to carry on.)\n"
        				+ "So, i see....   If you don't mind - what's your plan, kid? \n"
        				+ "What you are up to here now and in general?\n");
        		System.out.println("                        Abel:  This place grants great freedoms but with lots of inconveniences.\n"
        				+ "                               It is hard to collect usable paper that we can write on our \n"
        				+ "                               developing theorems. Books are also another issue - they are hard \n"
        				+ "                               to find, but with a lot of time-patience and wandering quests they\n"
        				+ "                               can be discovered. Sometimes only pages from them in unexpected \n"
        				+ "                               places. So here i am, i came here, to this sector, looking for a \n"
        				+ "                               papers and possibly some math, science books. Behind the doors of \n"
        				+ "                               this hallway you will find a whole, mostly intact library. A lucky \n"
        				+ "                               find. We did it ages of period-times ago. So i am up to staying here, \n"
        				+ "                               finally, for many time-periods for searches and library content \n"
        				+ "                               examination in hopes of finding anything that may help us in \n"
        				+ "                               our research and math theory developments.\n");
    		}
    		
    		System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{
    			System.out.println("Only now you actually noticed that \n"
        				+ "Abel held in one of his hands a thin stack of various \n"
        				+ "papers of various quality and state. And also he didn't \n"
        				+ "had any sort of bags or racks, he had to carry them in \n"
        				+ "his hands. - Must be tiring, you thought for yourself).\n"
        				+ "\nYou: \n"
        				+ "So, the quest of infinite intelligence, than? I can be \n"
        				+ "happy for you, sounds like you know what you are up to.\n"
        				+ "Can't be happy for myself, thou, at least until i can \n"
        				+ "assure myself i tried finding and securing my exit doors.\n"
        				+ "If they exist somewhere, somehow. Idea is - if there was a \n"
        				+ "way in, wouldn't there also be a ways out?\n"
        				+ "(And when you said that you felt happy of how smart you \n"
        				+ "must have been and sounded this moment. Tee-hee-heee :p )\n");
        		System.out.println("                        Abel:  It will be brave quest of you in trying something like that. \n"
        				+ "                               I wonder, what if you succeeded and indeed would prove it, \n"
        				+ "                               thus, that there are an exit points of some sort in existence here. \n"
        				+ "                               I tried for little bit of my time here but quickly gave up but \n"
        				+ "                               maybe because i started to like this place. \n");
        		System.out.println("You: \n"
        				+ "I will be moving forward.Thanks, kid, for some clarifications. \n"
        				+ "Hard to take this whole place in one go through one's head. \n"
        				+ "But i gotta move on, certainly, and see what further lies ahead.\n"
        				+ "Have to see with my own eyes.\n");
        		
        		System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
        		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
        	}
    		
    		
    		level5_atmospheric_hallway_first_visit_detection_state = false;
    		boolean check_player_has_math_paper_item = false;		// initialization of variable for further test
    		// now the player's inventory is being checked on presence of math paper item;
    		String item_code = "0";
    		String math_paper_item_from_level2 = "1";
    		int i = 0; int j = 0;
    		int a = 0; int b = 0;	// rvariables that will backup and save index of items position in inventory
    		for(j = 0; j <= 1; j++)
    		{
    		  i = 0;
    		  do
    		  {
    		    if( i == 1 || i == 4 || i == 7)
    		    {
    		      item_code = inventory[j][i];
    		      if (item_code.equals(math_paper_item_from_level2))		// if there is math-paper-item
    		      {
    		    	// test is positive, player indeed has this item  
    		       	check_player_has_math_paper_item = true;	
    		       	a = j; b = i;
    		       	break;
    		      }
    		  }
    		  i++;  
    		  } while ( i <= 7);
    		  System.out.println(); 
    		}
    		
    		if (check_player_has_math_paper_item == true && inventory[a][b].equals(math_paper_item_from_level2))
    		{
    			System.out.println("(Now that you said that, you wondered - what if that \n"
    				+ "picked-up paper with weird math mambo-jambo could be of great \n"
    				+ "value to this Abel-guy? Maybe he lost this paper and didn't \n"
    				+ "notice it? After all, seemingly he carries all those papers, \n"
    				+ "some sort of torn book pieces carefully in his hands. Must be \n"
    				+ "exhausting and some page might slip out after all at some point. \n"
    				+ "- Maybe i should give it out to him? - you think. ...\n"
    				+ "\n But then, again... What if maybe its best to keep it for myself \n"
    				+ "for now? What if i find this Lobachevsky mathematician myself somehow \n"
    				+ "... maybe then i could exchange this paper to some item or keys \n"
    				+ "or whatever great math master might turn in for exchange?\n\n"
    				+ "Besides, how can i be sure this Abel guy is real and not some mere \n"
    				+ "ghost, a trick of this place? \n"
    				+ "It certainly may be something supernatural, just a nightmare,  \n"
    				+ "this whole place is some kind of a hallucinatory joke, most likely.\n"
    				+ "And this kid might be next joke in a sequence of jokes. So,\n"
    				+ "fuck it, this Abel-guy ain't gonna get any math piece of paper or \n"
    				+ "anything from me. He can go fuck himself. If that's a game, i should \n"
    				+ "play it with some bargain, so to speak. But then again, he didn't \n"
    				+ "asked me of anything... \n"
    				+ "And what if that papers confuses young mind even more and he later \n"
    				+ "gets stuck even more with his theories?? \n"
    				+ "Hmmm... what should i do? Give paper now or save it for later use?");
    			System.out.println("--------------------------------------\n"
    				+ "1: Give the paper to Abel.\n"
    				+ "2: Save math paper for some later use.\n\n");
    			
    			player_choice = data_entry.nextInt();    		
    		
    			if (player_choice == 1)
    			{
    				System.out.println("------------------------------------------------------------\n"
    					+ "You: Ou, Abel, by the way! Before i go - i think you better take \n"
    					+ "     this from me. (You take out and hand over to him found math paper.) \n"
    					+ "     It's a paper i found back there in corridors. Maybe you lost it? \n");
    				
    				// some happy comments from Abel;
    				System.out.println("                        Abel:  (surprised and worried) !! What a misfortune, i lost them \n"
    						+ "                           without any notice ?! What a shame and unfortune it would be! This is \n"
    						+ "                           important finding, i have all suspicions that it even may be our greatest \n"
    						+ "                           finding so far - i have a firm suspicion that this paper comes from        \n"
    						+ "                           Mr. Lobachevsky's own book or research papers written by himself because \n"
    						+ "                           i recognize his handwritting style and language! My teacher doesn't know \n"
    						+ "                           of this finding yet because we don't have any distant communications and \n"
    						+ "                           his place now is many time period-eras far away. Also i was sent to do some \n"
    						+ "                           examinations of library and nearby rooms which will take long time. It would \n"
    						+ "                           be a catastrophe for me to realize a loss of this paper after my return! This \n"
    						+ "                           paper is important because it proves for first time that, perhaps, this place \n"
    						+ "                           may hold all our previous life's research papers which we can find, restore \n"
    						+ "                           and even continue beyond limits of our previous lifes!! So far we hadn't found \n"
    						+ "                           any - only the works of unknown, unrecognizable authors and sometimes unfamiliar \n"
    						+ "                           mathematic's fields which we never saw previously. This place is the paradise in \n"
    						+ "                           a disguise, then. \n"
    						+ "                           I thank you so much for that! \n");
    				System.out.println("You see how the Abel got so worried by this finding, \n"
    						+ "he couldn't stand in one place calmly and his movements were so \n"
    						+ "nervous that you yourself picked up little fraction of his drama.\n"
    						+ "\nYou: \n"
    						+ "Yes, Abel, take it. Sometimes we have to finish great \n"
    						+ "things but small, deceiving missteps can take us a lot \n"
    						+ "back as if whole universe conspired to make failure happen \n"
    						+ "as probabilistic as possible while leaving even small chance \n"
    						+ "of winning ,nevertheless, according to laws. Somehow i have \n"
    						+ "a feeling i know this...");
    				System.out.println("                        Abel:  here, let me give you this gift. I order you to not hesitate,\n"
    						+ "                           and accept it without any thinking. It is a key to doorway in a hall which \n"
    						+ "                           leads to unfamiliar territory which i planned to explore somewhat later in \n"
    						+ "                           hopes of finding something more and maybe some shortcut to my way back. \n"
    						+ "                           Consider that symbolically this route now belongs to you.\n");
    				
    				// math paper item (code number "1") is being removed from player's inventory;
    				// this slot gets new item - Abel's given key to door at level 8 - the hall;
    				inventory[a][b] = "3";
    				
    				System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    	    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    	    		if (key_enter.equals("c")) {	level8_first_hall();	}
    				
    			
    			}
    		
    			if (player_choice == 2)
    			{
    				System.out.println("------------------------------------------------------\n"
    						+ "You say to yourself - you never know. Besides, the kid probably \n"
    						+ "is already smart enough. Some kind of smart ass genious. He will \n"
    						+ "be fine without any bonus papers, i am sure. \n"
    						+ "Having said that, some portion, a back of your head recalled \n"
    						+ "something from the previous life's generic knowledge that sometimes \n"
    						+ "those math nerds may live in their problems for years without \n"
    						+ "progress because of some lack of a detail here or there. \n"
    						+ "- Naah, Abel-guy will be fine - you try to excuse yourself for \n"
    						+ "a possibly your very first greedy move in this new 'world'. \n\n"
    						+ "You proceed to the door at the end of this hallway.\n");
    				
    				System.out.println("... [press 'c' (and then 'Enter') to continue] ...");
    	    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    	    		if (key_enter.equals("c")) {	level8_first_hall();	}
    			}
    		}
    		else {	level8_first_hall();	}
    		
    	}
    	else	// else player enters this level in revisit mode, without a dialog with Abel
    	{
    		System.out.println("\n-------------------------------------------------------------------------------------\n"
					+ "You are at what seems like quite long, high-ceiling, very light kings and queens era's reminiscent\n"
					+ "hallway with one turn, meaning hallway is L-shaped. One side of this hallway in its segment before\n"
					+ "first turn is a wall without any windows but the rest of the segments on both wall sides are like \n"
					+ "windowed, glass walls.");
    		System.out.println("\n"
					+ "                        xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx- \n"
					+ "    to next area      < | D-7B                                         | \n"
					+ "                        xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-xxxxx-.    | \n"
					+ "                                                                  |    | \n"
					+ "                                                                  x    | \n"
					+ "                                                                  x    | \n"
					+ "                                                                  x    | \n"
					+ "                                                                  x    | \n"
					+ "                                                                  |    | \n"
					+ "                                                                  x    | \n"
					+ "                                                                  x    | \n"
					+ "                                                                  x y  | \n"
					+ "                                                                  |_  _| \n"
					+ "                                                                         \n"
					+ "                                                                    v    \n"
					+ "                                                        back to previous area \n\n");
    		System.out.println("Behind the windows you see that total darkness encompasses everything on one side.\n"
					+ "On opposite side windows through their glass you can see that some kind of gigantic \n"
					+ "rock cliff with some sort of walking roads and some structures is at some distance. \n"
					+ "The darkness encompasses everything there also but at some spots there is something \n"
					+ "like dim lighting spots. Still, from here you can't see much details.\n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c"))
    		{
    			// present user with options text block;
    			System.out.println("--------------------------------------\n"
        				+ "1: Go to next area (door D-7B).\n"
        				+ "2: Return to previous area \n\n");
    			
    			key_enter = "reset back to default state";
    			player_choice = data_entry.nextInt();    		
        		
    			if (player_choice == 1)	{	level8_first_hall();	}
    			if (player_choice == 2)
    			{
    				System.out.println("------------------------------------------------------------\n"
    						+ "You slowly approached the door that leads back to previous room. It seems \n"
    						+ "Abel went somewhere next. You decided for yourself that corridors and rooms \n"
    						+ "back there most likely don't hold great importance to you now and you feel \n"
    						+ "moved to go forward from now on. You decidedly don't want to confront your \n"
    						+ "entry into this world or dream, whatever it is, you want to get further.\n\n"
    						+ "You turn around and take a glimpse of hallway once again. -- What a mistery. \n"
    						+ "Looks truly impressive. If it's a dream or illusion, it is with atmosphere -- \n"
    						+ "you said to yourself. Then you go. Suddenly, somewhere far, in darkness void \n"
    						+ "you catched a glimpse of what seemed to you like a quick lighting strike. \n"
    						+ "It happened so fast, that you catched it only partially. But you stand here sure \n"
    						+ "that it was like lighting; maybe it was a lighting, which might mean normal\n"
    						+ "physics and something like a real world.\n");
    				
    				// preparing another text block section's pause for player's readability
    				System.out.println("... press 'c' (and then 'Enter') to continue ...");
    				key_enter = data_entry.next();
    				if (key_enter.equals("c")) {	level8_first_hall();	}
    			   				
    			}
    		}
    		
    	}
    }
    
    public void level6_philosophers_room()
	{
    	System.out.println("-----------------------------------------------------\n"
    			+ "You stand in what appears another empty interior room. There is no \n"
    			+ "lighting in this room, only the light from cabinet that gets in allows \n"
    			+ "you to see something around here. Room has no other doors that could \n"
    			+ "lead somewhere else. In little light that gets into room you can see \n"
    			+ "that there is sofa, facing what probably is windows array which you \n"
    			+ "guess by little refractive effects on them. You step carefully into \n"
    			+ "room and slowly examine its space. Through the prolonged windows array \n"
    			+ "you see the same all-encompassing pitch-black darkness that doesn't \n"
    			+ "want to reveal anything from an exterior world's landscape. \n"
    			+ "- Philosopher's paradise - you think to yourself jokingly. \n"
    			+ "You decide to go back. \n");
    	
    	System.out.println("... press 'c' (and then 'Enter') to continue ...");
		key_enter = data_entry.next();
		if (key_enter.equals("c")) {	level4_room_with_curiosities();	}
    }
    
    public void level7_special_observations_place()
    {
    	System.out.println("---------------------------------------------------------------\n"
    			+ "                                                                        \n"
    			+ "       |     back to cabinet                                 \n"
    			+ "       |           ^                                |        \n"
    			+ "       --------- ^v^w   ----------------------------   \n"
    			+ "               |________|                      \n"
    			+ "               |________|                      \n"
    			+ "               |________.                      \n"
    			+ "               |    ___.\\                      \n"
    			+ "               \\^-^   .^ /^----------.        \n"
    			+ "                l    .^  /  |  |   /   ^.      \n"
    			+ "                 l..^   /   |  |  /  ./ |      \n"
    			+ "                   ^-----------._/ .^   |      \n"
    			+ "                                ^.^_____|      \n"
    			+ "                                |_______|      \n"
    			+ "                               //^._    |      \n"
    			+ "                             .^ \\   -._/      \n"
    			+ "                          _-^-_   \\_-^      \n"
    			+ "                _ _ _ _ _-^    ^_  ./      \n"
    			+ "               |                  |      \n"
    			+ "               |                  |    \n"
    			+ "               |_.        y       |    \n"
    			+ "                  ^x              |    \n"
    			+ "                    x             |    \n"
    			+ "               . xs^              /    \n"
    			+ "               |x x_ _ _ ________/     \n");
    	
    	System.out.println("You discovered stairway that leads somewhere. You went all way down \n"
    			+ "and got to the room who's one corner is missing and seems structurally destroyed \n"
    			+ "by some force. Structural hole is so huge - it literally made this room more like \n"
    			+ "an 'improvised' balcony for the outside view than an interior room. And as if to \n"
    			+ "underscore this idea, seems, somebody left here a small sofa. You wonder, who \n"
    			+ "could place it here and for what purpose - to look at pitch-black darkness all over \n"
    			+ "around? - Somebody went through some efforts to bring in sofa here, after all..- \n"
    			+ "you wonder to yourself. You think - maybe the darkness, the sky does change \n"
    			+ "sometimes, and whoever organized this place was determined to catch a glimpse of \n"
    			+ "something in those endless dark skies? Nevertheless, you decide to go back. \n");
    	
    	System.out.println("... press 'c' (and then 'Enter') to continue ...");
		key_enter = data_entry.next();
		if (key_enter.equals("c")) {	level4_room_with_curiosities();	}
    	
    }
    
    public void level8_first_hall()
    {
    	player_choice = 0;	// nullify the value to reset it for safety
    	player_location = 8;
    	int[] level8_context_possibilities = {1,3};		// possibility to [1]use [3]Abel's given key
    	System.out.println("-------------------------------------------------------------------------------------------\n"
    					+ "You are in a place that seems like great hall with high ceilings, four huge ancient civilization's \n"
    					+ "era columns, marble floor that looks like some sort of a work of art. Lighting is incredibly \n"
    					+ "light in the hall. Feeling is impresive and optimistic - it is as if you got a chance to time  \n"
    					+ "travel and see some portion of just newly built ancient hallway of some temple or building.");
    	System.out.println("--------------------------------------------------------------------------------------------\n"
    			+ "            --------..     --  ---                     \n"
    			+ "            | D-8B    *.   |  /  |                     \n"
    			+ "            ------.     |  |D-8C |                     \n"
    			+ "                   !    |  |     |          D-8D       \n"
    			+ "                   |    |  |     |      _ _ ____ _ _   \n"
    			+ "                   |    \\__|     |_ _ _| oo      oo |  \n"
    			+ "                   |      .                          ----------   \n"
    			+ "                   |      _                                    |  \n"
    			+ "                   |     |_|                                   |   ------------------------------    \n"
    			+ "                   |            .oo.                .oo.       |  |                              |    \n"
    			+ "                   |      .    o.  .o              o.  .o      |  |                              |    \n"
    			+ "                   |            *oo*                *oo*       |  |     some library             |    \n"
    			+ "                   }      .                                    |  |                              |    \n"
    			+ "                   } D-8E                                       --                               |    \n"
    			+ "                   |      _                                                                      |    \n"
    			+ "                   |     |_|                                    --.                              |    \n"
    			+ "                   |                                           |  |                              .    \n"
    			+ "                   }      .                                    | \\                              /   \n"
    			+ "                   } D-8F                                      |   *._ _ _ _ _ _ _ _ _ _ _ _ _.*    \n"
    			+ "                   |      .     .oo.                .oo.       |                                 \n"
    			+ "                   |           o.  .o              o.  .o      |                  stairs         \n"
    			+ "                   |      .     *oo*                *oo*       |              |-----------|        \n"
    			+ "                   |      _                                    |             .* /--x -_.  |        \n"
    			+ "                   |     |_|                                    ------------* ;;  xl-l    |        \n"
    			+ "                   |      .                                       | | | | |.\\  x o l-    /         \n"
    			+ "    .--------------       .                y                      | | | | | l\\  ;^il    /         \n"
    			+ "    | D-8A                .                                       | | | | |  .\\    ;  .*          \n"
    			+ "    -------------------------------------  v  ----------------------------------------*          \n"
    			+ "                                  return to previous area                                       \n");
    	
    	System.out.println("------------------------------------\n"
				+ "1: Take a look around the hall.\n"
				+ "2: Approach the door D-8A.\n"
				+ "3: Approach the door D-8B.\n"
				+ "4: Go to the door D-8C (it is open).\n"
				+ "5: Approach the door D-8D.\n"
				+ "6: Approach the door D-8E.\n"
				+ "7: Approach the door D-8F.\n"
				+ "8: Return back (to previous area > hallway).\n"
				+ "9: Check inventory.\n");

    	player_choice = data_entry.nextInt();
    	
    	if (player_choice == 1)
    	{
    		System.out.println("-----------------------------------\n"
    				+ "You observe to your right and see quite large stairway leading somewhere and turning left at \n"
    				+ "some point. You are visually shocked to observe that stairway is being blocked by debris - \n"
    				+ "quite huge rock blocks, colorific geologic rocks that you don't recognize and a huge carcas \n"
    				+ "of skeletal remains of some huge creature; as if it could be remains of some dinosaur! \n"
    				+ "- How it got here, this mess? What's going on? - you think to yourself. In library you find \n"
    				+ "quite large shelves filled with books, there are comfortable sitting sofas, tables, carpets,\n"
    				+ "lighting, but no persons occupying some of that space. - Probably that's the library that Abel \n"
    				+ "talked about. It must take many months if not years to profile what this whole library is \n"
    				+ "about. How is he gonna do that? - you think again to yourself. In hall it's left side surprises \n"
    				+ "you with something. It is as if it's left side is totally from other century, a modern days that \n"
    				+ "you are familiar with. Starting with the corridor to the door D-8A and following along the left \n"
    				+ "side hall's wall, this segment's look resembles more like an office/hospital reception floor's \n"
    				+ "furniture. The ceiling here also lowers down to usual 2+ meters. Double doors D-8F look like a g \n"
    				+ "revolving doors at some hotels. Double doors D-8E look like wood panel doors with some kind of \n"
    				+ "wood faces carvings done artistically. Corner that ends with door D-8B looks like usual office \n"
    				+ "or hospital corridors, only here on wall you spot a frame with a painting or drawing behind the \n"
    				+ "glass - something abstract with color blobs and some sort of alien-like figure sitting on bottom \n"
    				+ "side and thinking something. Door's D-8C area, again, looks totally like from other building and \n"
    				+ "era. It somewhat chaotically 'stitches' to the hall, and everything here is of wood planks and wood \n"
    				+ "looks fairly old, you see cracks here and there. But the door is opened and there is some slightly \n"
    				+ "lighted passage that you see and which takes a turn somewhere, obscuring what is further there.\n"
    				+ "Finally, huge double doors D-8D looks heavy and as if belonging to hall architecture's style. \n"
    				+ "You get back to hall's center and think what to do next. \n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c")) {	level8_first_hall();	}
    	}
    	
    	if (player_choice == 2)
    	{
    		System.out.println("-----------------------------------\n"
    				+ "The door is missing it's handle. You try pushing it with your body \n"
    				+ "mass but the door won't open or move. It is tightly locked and fixed \n"
    				+ "in its position. Nothing can be done here for now. \n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c")) {	level8_first_hall();	}
    	}
    	
    	if (player_choice == 3)
    	{
    		System.out.println("You are at the door.");
    		System.out.println("-------------------------------\n"
    				+ "1: Return to hall's main square.\n"
    				+ "9: Check inventory.\n");
    		
    		player_choice = data_entry.nextInt();
    		
    		if (player_choice == 1) {	level8_first_hall();	}
    		if (player_choice == 9)
    		{
    			player_choice = 0;		// zero out to reset variable to prevent it from "leaking" through system
    			player_opens_inventory(level8_context_possibilities, player_location);
    		}
    	}
    	
    	if (player_choice == 4)
    	{
    		System.out.println("-------------------------------------------------\n"
    				+ "You approach opened doorway. Wooden part of interior near this door reminded you \n"
    				+ "some interiors from a cowboy western  movies, some kind of a barn's or maybe some \n"
    				+ "wooden pantry's or bar's sections. Suddenly, as you step and before you fully realize \n"
    				+ "what happens, you hear growing sound of wooden cracks and feel that you start 'loosing \n"
    				+ "the ground' under your feets. You fall down one level below and when hitting upon what \n"
    				+ "seemed another wood plank ground, you heard yet another sound of cracks and in a second \n"
    				+ "you felt that your fly to the bottom of whatever continued. Now you landed on some sort \n"
    				+ "of a surface of an object which started to slide down by inertia, and together with some \n"
    				+ "old boxes and cardboards you finally land on more concrete floor. You looked up, from \n"
    				+ "where you supposedly started to fall down and you see that there is no way of getting \n"
    				+ "back there directly. You are in what seems like a basement corridor with some distant doors \n"
    				+ "further away. \n\n"
    				+ "- Well, at least basement with electricity and garbage stuff reminds me of my century that \n"
    				+ "i lived in - you say to yourself. You decide that this is a spot where from now on you start \n"
    				+ "your true quest of looking into more carefully what this place is, how you probably got here \n"
    				+ "and maybe - how to get out of it, as well. \n");
    		
    		player_choice = 0;
    		// demo game's ending reached; scenario B
    		System.out.println(player_name + ", you understand now that quest trully will be long and \n"
    				+ "it only has started. You will need to explore this basement's corridors and doors \n"
    				+ "to reach better conclusion of where to go next and whether seek ways how to get \n"
    				+ "back on upper levels from which ones you had a misfortune to fall down. ");
    		System.out.println("           ...........                \n");
        	System.out.println("Game ending stats:  \n"
        			+ "player name: " + player_name + "\n"
        			+ "ending scenario: C");
    	}
    	
    	
    	if (player_choice == 5)
    	{
    		System.out.println("-----------------------------------------------------------------------------\n"
    				+ "Doors seemed huge and heavy but they opened up when you pushed them. What you stared \n"
    				+ "at now was something like a road that went far, far away somewhere - into the darkness. \n"
    				+ "Only this time a hanged light-emitting object (by some gracious electrician, engineer or \n"
    				+ "whatever gracious soul did that, you think to yourself) on the exterior wall of hall's \n"
    				+ "building complex revealed some nerby teritory. You saw that somewhere in the distance \n"
    				+ "on sides of the road there were some structures resembling old city square. Light didn't \n"
    				+ "get so far to reveal detailed look but judging from where you standed and from absence \n"
    				+ "of any indoor lighting in any of houses - there were no signs of life or other human \n"
    				+ "immediate presence. - If Abel arrived from this direction he must be one brave soul. \n"
    				+ "You decide that unknown scale city blocks with unknown street crossings and overall \n"
    				+ "darkness might be too risky place for visit now before you settle on more details and \n"
    				+ "some equipment or knowledge about this whole place. You decide not to go this way. \n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c")) {	level8_first_hall();	}
    	}
    	
    	if (player_choice == 6)
    	{
    		System.out.println("No good, this double door is firmly shut as well. Can't open it. \n"
    				+ "Not even with force. You wonder what's behind it. \n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c")) {	level8_first_hall();	}
    		
    	}
    	
    	if (player_choice == 7)
    	{
    		System.out.println("The moment you try to push doors away from you, they slightly \n"
    				+ "move and then you feel you can't push them any further. Most likely there \n"
    				+ "is some kind of object put across revolving doors handle niches at the other \n"
    				+ "side to stop them from opening. Probably somebody's joke, a way of securing a \n"
    				+ "path only for themselves or some other scenario. For now, no hopes of opening \n"
    				+ "them. \n");
    		
    		System.out.println("... press 'c' (and then 'Enter') to continue ...");
    		key_enter = data_entry.next();	// variable is prepared and opened to accept keyboard symbols from user
    		if (key_enter.equals("c")) {	level8_first_hall();	}
    		
    	}
    	
    	if (player_choice == 8)
    	{
    		// if player chooses to return, he returns to level of atmospheric hallway under 
    		// condition that it is registeret that it is repeated visit (which brings scenario effects);
    		level5_atmospheric_hallway(level5_atmospheric_hallway_first_visit_detection_state);
    	}
    	
    	if (player_choice == 9)
    	{
    		// make player_location an abstract and neutral value to ensure entering inventory in neutral mode;
    		// just some number, neutral enough and not equal to key-value 8 for puzzles in this area; 
    		player_location = -8;
    		player_choice = 0;	// after everything, value is being reset
    		player_opens_inventory(level8_context_possibilities, player_location);
    		level8_first_hall();
    	}
    }
    
    public void level10_the_road_somewhere()
    {
    	player_choice = 0;	// zero-out, reset variable for non-interference guarantee with game structure
    	System.out.println("\nYou stepped outside into some kind of street road which comes close \n"
    			+ "almost to the door. The road slightly gives turn to the right and on the right \n"
    			+ "side of it is a rock cliff wall which increased in height as the road goes \n"
    			+ "down while turning. On the left side is a road border, as you know them in \n"
    			+ "cities that you remember from your previous real-life. Beyong it is a sharp \n"
    			+ "cliff right into dark, endless void where nothing can be seen. This is the road \n"
    			+ "that leads somewhere, it is a two lane driving road, only there are no cars. \n"
    			+ "It was as to watch some spectacular apocalypse scene only with addition that \n"
    			+ "natural skies and sun were 'wiped out' as well, making total darkness leaving \n"
    			+ "only some ambient and artificial lights on. And indeed you could spot few, laid \n"
    			+ "out in huge distances in-between them, some kind of artificial dim-light emitters \n"
    			+ "who at least produced barely minimal light to enable start seeing main outlines \n"
    			+ "of objects and space. This road reminds you of your past reality and you decide \n"
    			+ "to go further and proceed in this direction, feeling more confident that it will \n"
    			+ "certainly be a road to somewhere. In far distance you could recognize a tall shape \n"
    			+ "of bridge. An industrial engineering structure! \n\n"
    			+ "You know that your true quest, finally, has consciously started. \n");
    	
    	System.out.println("           ...........                \n");
    	System.out.println("Game ending stats:  \n"
    			+ "player name: " + player_name + "\n"
    			+ "ending scenario: A");
    }
}