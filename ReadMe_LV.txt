"Epizode Purgatorijā" ir tekstā bāzēta spēle, kurā spēlētājs vada personāžu ar atmiņas
pagaidu zaudējumu. Personāžs mēģina atjaunot savas vēlākās atmiņas un noskaidrot savu
nonākšanu šķietami sireālajā vietā, veicot darbības tekošā brīža pasaulē. Darbības vieta
metaforiski tiek pielīdzināta Purgatorijam. Spēlē ir vairāk nekā viens beigu scenārijs,
kuru spēlētājam ir iespējams sasniegt atkarībā no savām darbībām un izvēlēm.

Spēle nav gara un kalpo vairāk kā demonstrācijas variants. Spēles dzinējs nenodrošina
pilnīgu drošību un noturību pret spēlētāja kļūdām, piemēram, tam ievadot burtus prasīto
ciparu vietā. Spēlēs veicamo izvēļu apstiprinājumi jāveic uzmanīgi - jāievada tikai tos
veselos, naturālos skaitļus, kas piedāvāti zem rīcību opcijām, lai sekmēt normālu spēles
procesu. Kopumā, spēle ir spēlējama un ir sasniedzami visi iespējamie beigu scenāriji.
Spēlē ir ieviesta pamat-inventorija sistēma. Avot-kods satur daudzus komentārus, kas
paskaidro spēles dzinēja mainīgos un loģisko mehāniku. Kods var būt interesants tiem, kas
vēlas ieraudzīt šīs teksta spēles dzinēju vai pat uzlabot to saviem projektiem. Tekošajā
stāvoklī, pēc iepazīšanās ar kodu, spēles dzinējs pieļauj salīdzinoši vienkāršu spēles
pasaules paplašināšanu ar jauniem līmeņiem, dialogiem un artifaktiem.

Spēles programmas, stāsta un stāsta pasaules konceptuālās būtības autors - Intars Kočeševs.
Spēles stāsts ir variācija, atzars no personīga mākslas hobija projekta. Spēles programmas
pamatelementi tikuši programmēti paralēli studijām RTU Datorzinātnes fakultātē/ Robotikas
studiju programmā sava personiskā papildtreniņa ietvaros pirmajos semestros, 
2017.-2018.gada ziemā. Spēles programma tikusi pieslīpēta, papildināta un pabeigta
novembrī - decembrī 2021. gadā.

Teksta spēle ir saprogrammēta programmēšanas valodā Java, tās 8.versijā (Java 8 SE).
Precīzs lietotās versijas numurs: Java - version 8 update 144 (build 1.8.0_0144-b01).
Koda izstrādei un spēles testiem lietotā progr.vide:
Eclipse IDE - version: Oxygen.1a Release (4.7.1a).

Spēles programmas kods ir bezmaksas, atvērtais kods jebkādiem lietojumiem.
Stāsta/ sižetiskās izdomas un mākslinieciskā koncepta autortiesības ir autora
(Intars Kočeševs) intelektuālais īpašums.


-------------------------   Kā palaist spēli  ------------------------------------
Programmētājiem:
  savā kompī jebkurā IDE, kas atbalsta Java valodas kompilāciju, atvērt/iekopēt
  "Purgatory_episode_text_adventure_game.java" failu. Tad nokompilēt un palaist.

Ne-programmētājiem:
1. veids:

[1] Jādabū un jāuzinstalē neliela, bezmaksas programma "DrJava";
  to var dabūt interneta vietnē: http://www.drjava.org/
  Tā ļauj ātri un viegli palaist (arī izstrādāt) kodu Java valodā.

[2] Iekš DrJava:  vai nu ar peli jāpārvelk kreisajā panelī spēles koda nokačātais fails
  "Purgatory_episode_text_adventure_game.java"   vai arī, ja nav pārliecības

  File > Open ..(aiziet uz nokačāto spēles mapi).. -->
  --> izvēlēties atvērt "Purgatory_episode_text_adventure_game.java" -->
  --> tad uz DrJava rīkjoslas sameklēt un nospiest "Compile" > un tad "Run".

[3] Dotajā brīdī, ja viss veiksmīgi, spēlei būtu jāpalaižas. Atliek pavilkt DrJava
  teksta izvades konsoles loga (Interactions) robežu uz augšu, lai palielināt spēles 
  "ekrānu".
  ----
  Ja DrJava tomēr nevar nokompilēt (apstrādāt) spēles koda failu, tas ar lielām varbūtībām
  nozīmē, ka dators nav aprīkots ar visu nepieciešamo Java valodas programmu apstrādei.
  Tas ir neērts scenārijs, jo paredz, ka būs jāuzstāda oficiālā, bezmaksas Java valodas
  apstrādes/palaišanas rīks (Java Runtime Environment, īsināti JRE) no Oracle kompānijas.

........
2. veids: palaist spēles koda failu kādā no Java valodas on-line kompilatoriem;
  viens no šādiem bezmaksas on-line Java valodas kompilatoriem ir: 

  https://www.online-ide.com/online_java_compiler

  Vietnē vai nu uzklikšķina ar peli uz mapes ikonu ("Open File from Disk") un tālāk
  aiziet līdz nokačātajam spēles failam uz sava datora

  vai arī

  atver spēles koda failu jebkurā teksta redaktorā, piemēram, notepadā, iezīmē pilnīgi
  visu faila saturu (vislabāk izdarīt ar Ctrl+A) un iekopē on-line vietnē, jaunizveidotajā
  (nospiest "+") Java koda lapā. Svarīgi pēc tam ir pārdēvēt jaunizveidoto lapu no defoultā
  "Untitled1" precīzi uz spēles faila nosaukumam atbilstošo "Purgatory_episode_text_adventure_game"
  (šoreiz bez  ".java" paplašinājuma daļas)!. Tad spiež "Run", palielina izvades ekrāna daļas
  izmēru un iet cauri spēles procesiem.
  Jāpatur prātā, ka šie bezmaksas on-line kompilatori ir ar savām kaprīzem - tie neļauj ilgi
  turēt programmu aktīvu bez jebkādas aktivitātes, diemžēl. Daži online kompilatori var
  atslēgt aktīvo programmu jau pēc 10 sekundžu neaktivitātes, citi pēc ilgāka laika. Toties
  šis scenārijs neparedz datora aprīkošanu ar Java valodas apstrādes oficiāliem rīkiem.