"Purgatory episode" is a text-adventure game in which you play a character with a loss of
memory and who tries to make some sense of his newly found environment by taking action
and explorations. Character finds himself supposedly in alternate, distinct from previous
real-life world, reality metaphorically called Purgatory. Game has more than one possible
ending scenario determined by player's actions.

The game engine is not totally completed and 'bullet-proof' against user choice's logical
errors. Game option choices must be made quite carefully - only integer numbers from numbered
options during gameplay must be entered to ensure normal play.Still, game normally is playable
and all ending scenarios reachable. Game features basic inventory system. Source code has many
comments that reflects logic mechanics of the game and might be of interest to anyone curious,
looking of how to create something similar or even better.

Game program, story and it's concept's author - Intars Kocesevs.
Game's story is a conceptual spin-off from one of my special artistic hobby project. Basics of
game engine was programmed as a means of additional programming practice during university
studies in computer science/robotics study course in first semesters back in winter 2017-2018.
The whole game code was polished, extended and finished in Nov-Dec 2021. 

This text-adventure was programmed using Java - version 8 update 144 (build 1.8.0_0144-b01).
For code development and its run was used Eclipse IDE - version: Oxygen.1a Release (4.7.1a).

Game code is free and open-source for any use.
Story/plot/artistic concept is copyright of (c) Intars Kocesevs, 2021.


-----------------------------  How to run/play it  ---------------------------------
For programmers:
  from downloaded folder extract java file "Purgatory_episode_text_adventure_game.java"
  and compile, then run it in any IDE with Java language support or through command prompt.

For non-programmers:
.........
1. way of running the game on your computer:

[1] get and install very small, lightweight, free program called Dr.Java;
  get it from web on: http://www.drjava.org/
  It allows to run (as well as develop) Java programming language code.

[2] Inside DrJava: either drag and drop into left side pane game code file named
  "Purgatory_episode_text_adventure_game.java" or, if unsure

File > Open > ..(go to downloaded game folder).. >-- 
  --> find and choose to open "Purgatory_episode_text_adventure_game.java" >--
  --> then on DrJava toolbar find and press "Compile" > press "Run" on the same toolbar. 

[3] Hopefully by now you have game successfully launched. Extend DrJava user interface's
  lower part up by dragging mouse on horizontal UI pane separation line so you have more
  screenspace for game process output (in so called Interactions pane). 
  ----
  If DrJava can't compile Java code, it means you might need to install official, free
  Java Runtime Environment (JRE) which contains everything needed to enable Java language
  programs in your computer.

........
2. run the code file "Purgatory_episode_text_adventure_game.java" in web-browser-based
Java programming language compilers. One such option is at:

 https://www.online-ide.com/online_java_compiler

[1] either press on folder icon "Open File from Disk" and go / open downloaded
"Purgatory_episode_text_adventure_game.java"  

 or   

open downloaded code in any text editor (like notepad, for example),
Copy-Paste all code lines and paste them into newly made (press "+" tab) Java code
space in browser (after that renaming newly made code space tab from the default
"Untitled1" precisely to "Purgatory_episode_text_adventure_game"  ;

 > then hit "Run" > adjust output console window and play the game.
  